package adn;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

/**
 * Clase que se encarga de leer un archivo txt y guardar su contenido en una variable
 * @author Andrés Tenesaca Burgos
 * @version 1.0
 * @data 21/11/18
 */
public class Archivo {
    /**
     * funcion que lee el archivo y lo guarda en un string
     *
     * @return cadena el contenido del archivo
     * @throws FileNotFoundException excepcion de error de lectura de archivo
     */
    public static String leerArchivo() throws FileNotFoundException {
        //crea un objecto que contiene la ruta o nombre del archivo
        File archivo = new File("adn.txt");

        //cadena que contendra el archivo
        String cadena = null;

        //objecto para leer el archivo
        FileReader f = new FileReader(archivo);
        Scanner sc = new Scanner(f);
        //mientras haya una linea
        while (sc.hasNextLine()) {
            //cadena guarda el contenido
            cadena = sc.nextLine();
        }
        //devuelve cadena
        return cadena;
    }
}