package adn;

//zona de paquetes
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

/**
 * Archivo que contiene el menu del adn
 * @author Andrés Tenesaca Burgos
 * @version 1.0
 * @data 23/10/18
 */
public class Adn {
    //Variable cadena que contendra el contenido del fichero adn.txt
    String cadena;
    
    /**
     * contructor vacio que inicializa la variable cadena al contenido del txt
     * @throws FileNotFoundException excepcion de lectura de fichero
     */
    public Adn() throws FileNotFoundException {
        this.cadena = Archivo.leerArchivo();
    }
    
    /**
     * Array que contiene las opciones del menu
     */
    private String[] menu = {
        "Borrar",
        "Buscar",
        "Salir"
    };
    
    /**
     * Main que crea un objecto adn y crea una funcion para llamar al menu
     * @param args
     * @throws IOException excepcion de lectura de fichero
     */
    public static void main(String[] args) throws IOException {
        Adn adn = new Adn();
        adn.run();
    }
    
    /**
     * funcion que se encarga de la seleccion de las diferentes opciones del menu
     * @throws IOException excepcion de lectura de fichero
     */
    private void run() throws IOException{
        //opcion por defecto para salir
        int optionSelect = 3;
        //mientras opcion sea diferente a 3
        do{
            optionSelect = showMenu();
            //opciones del menu
            switch(optionSelect){
                case 1: System.out.println("Borrar");
                        break;
                case 2: System.out.println("Buscar");
                        break;
                case 3: System.out.println("Has salido del menu");
                        break;
                default:
                    System.out.println("Opcion incorrecta");
                        break;
            }
        }while(optionSelect!=3);
    }
    /**
     * funcion que se encarga de mostrar el menu y
     * pregunta al usuario la opcion a elegir
     * @return opcion elegida por el usuario
     */
    private int showMenu() {
        int option;
        //recorre el array e imprime las diferentes opciones
        System.out.println("Menu");
        for (int i = 0; i < menu.length; i++) {
            System.out.format("%d %s \n", i+1, menu[i]);
        }
        //crea un objecto sc para poder escribir por teclado
        Scanner sc = new Scanner(System.in);
        //seleccion una pocion del menu
        System.out.print("Choose an option: ");
        option = sc.nextInt();
        //devuelve la opcion elegida
        return option;
    }
    
    boolean existe(String cadenaABuscar) {
        boolean result = false;
        if(cadena.contains(cadenaABuscar)) {
            result = true;
        }
        return result;
    }

    int buscar(String cadenaABuscar) {
        int cont = 0;
        if(cadena.contains(cadenaABuscar)){
            cont++;
        }
        return cont;
    }
    
}
