package adn;

import java.io.FileNotFoundException;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Archivo que contiene los test de la clase ADN
 * @author Andrés Tenesaca Burgos
 * @date 21/11/18
 * @version 1.0
 */
public class AdnTest {
    
    public AdnTest() {
    }
    //Tenemos que poner el @Test para que el programa sepa que tendra que ejecutarlo
    @Test
    public void buscarSecuencia() throws FileNotFoundException {
        Adn adn = new Adn();
        assertTrue(adn.existe("ATCG"));
        assertTrue(adn.buscar("ATCG") == 1);
    }
}